package actividad6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad6 {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Estudiante 1");
        Student student1 = getNewStudent();
        System.out.println("Estudiante 2");
        Student student2 = getNewStudent();

        System.out.println(student1);
        System.out.println(student2);

        if (student1.compareTo(student2) > 0) {
            System.out.printf("El estudiante %s es mayor que el estudiante %s\n",
                    student1.getName(), student2.getName());
        } else if(student1.compareTo(student2) < 0){
            System.out.printf("El estudiante %s es mayor que el estudiante %s\n",
                    student2.getName(), student1.getName());
        } else {
            System.out.println("Los dos tienen la misma edad");
        }
        ArrayList<Student> alumnosDam = new ArrayList<>();
        alumnosDam.add(student1);
        alumnosDam.add(student2);
        Collections.sort(alumnosDam);
        System.out.println(alumnosDam);
    }

    private static Student getNewStudent() {
        String name = getInputName();
        int age = getInputAge();
        float height = getInputHeight();
        return new Student(name, age, height);
    }

    private static String getInputName(){
        System.out.println("Introduce el nombre");
        return scanner.next();
    }

    private static int getInputAge() {
        do {
            try {
                System.out.println("Introduce la edad");
                return scanner.nextInt();
            }catch (InputMismatchException e){
                System.out.println("Debe introducir una edad válida");
            } finally {
                scanner.nextLine();
            }
        }while (true);
    }

    private static float getInputHeight() {
        try{
            System.out.println("Introduce la altura");
            return scanner.nextFloat();
        }catch (InputMismatchException e){
            System.out.println("Debe introducir una altura válida");
            return getInputHeight();
        } finally {
            scanner.nextLine();
        }

    }

}

