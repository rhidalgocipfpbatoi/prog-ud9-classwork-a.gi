package actividad2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad2 {

    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {

        int maximo = Integer.MIN_VALUE;
        for (int i = 0; i < 6; i++) {
            try {
                int numero = obtenerEntero();
                if (numero > maximo) {
                    maximo = numero;
                }
            } catch (InputMismatchException e) {
                System.out.println("Dato erróneo");
                scanner.next();
                i--;
            }
        }

        System.out.println("El máximo número es el " + maximo);
    }

    private static int obtenerEntero() {

        System.out.println("Inserta un número entero:");
        return scanner.nextInt();
    }
}
