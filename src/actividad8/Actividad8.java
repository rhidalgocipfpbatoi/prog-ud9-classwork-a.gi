package actividad8;

public class Actividad8 {

    public static void main(String[] args) {
        try {
            waitSeconds(10);
            System.out.println("Fin");
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
    private static void waitSeconds(int seconds) throws InterruptedException {
        Thread.sleep(seconds * 1000);
    }
}
