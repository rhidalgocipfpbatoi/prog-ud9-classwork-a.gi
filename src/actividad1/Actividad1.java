package actividad1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int maximo = Integer.MIN_VALUE;
        for (int i = 0; i < 6; i++) {
            try {
                int numero = scanner.nextInt();
                if (numero > maximo) {
                    maximo = numero;
                }
            } catch (InputMismatchException e) {
                System.out.println("Dato erróneo");
                scanner.next();
                i--;
            }
        }

        System.out.println("El máximo número es el " + maximo);
    }

}
