package actividad9;

import actividad9.exceptions.TanqueLlenoException;
import actividad9.exceptions.TanqueVacioException;

public class TestTanque {
    public static void main(String[] args) {
        Tanque tanque = new Tanque(100);
        Tanque tanque2 = new Tanque(20);

        try {
            tanque.agregarCarga(90);
            tanque.agregarCarga(20);
        } catch (TanqueLlenoException e) {
            System.out.println(e.getMessage());
        }

        try {
            tanque2.retirarCarga(10);
            tanque2.agregarCarga(10);
            tanque2.retirarCarga(5);
        } catch (TanqueVacioException | TanqueLlenoException e) {
            System.out.println(e.getMessage());
        }

        System.out.println(tanque);
        System.out.println(tanque2);
    }
}
