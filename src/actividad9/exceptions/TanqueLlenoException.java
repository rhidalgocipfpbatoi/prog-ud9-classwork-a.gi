package actividad9.exceptions;

public class TanqueLlenoException extends Exception{
    public TanqueLlenoException() {
        super("El tanque está lleno");
    }
}
